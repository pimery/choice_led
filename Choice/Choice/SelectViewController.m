//
//  SelectViewController.m
//  choice_2
//
//  Created by hgu on 2016. 8. 22..
//  Copyright © 2016년 hgu. All rights reserved.
//

#import "SelectViewController.h"
#import "ViewController.h"

@interface SelectViewController ()

@end

@implementation SelectViewController
@synthesize eatBtn, buyBtn, imgView;

- (void)viewDidLoad {
    [super viewDidLoad];
    eatBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    buyBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;

}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [NSThread sleepForTimeInterval:0.1];
    
    if([segue.identifier isEqualToString:@"choiceEat"]){
        ViewController *controller = (ViewController *)segue.destinationViewController;
        controller.EorB = @"E";
    }
    else {
        ViewController *controller = (ViewController *)segue.destinationViewController;
        controller.EorB = @"B";
    }

}

@end
