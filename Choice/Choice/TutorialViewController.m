//
//  TutorialViewController.m
//  choice_2
//
//  Created by hgu on 2016. 8. 22..
//  Copyright © 2016년 hgu. All rights reserved.
//

#import "TutorialViewController.h"

@interface TutorialViewController ()

@end

@implementation TutorialViewController

@synthesize exitBtn, imgView;

- (void)viewDidLoad {
    [super viewDidLoad];
    exitBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [NSThread sleepForTimeInterval:0.1];
}

@end
