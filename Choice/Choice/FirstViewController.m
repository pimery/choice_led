//
//  FirstViewController.m
//  choice_2
//
//  Created by hgu on 2016. 8. 14..
//  Copyright © 2016년 hgu. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController
@synthesize img, startBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    startBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"연결할 주소를 입력해주세요." message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        NSString *val = ((UITextField *)[alert.textFields objectAtIndex:0]).text;
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:val forKey:@"USER_HOST"];
        [defaults synchronize];
    }];
    [alert addAction:okAction];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.text = @"192.168.0.18";
    }];
    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc presentViewController:alert animated:YES completion:nil];

}
- (void) viewWillAppear:(BOOL)animated {
    [UIView animateWithDuration:0.1f animations:^{
        img.animationImages = [NSArray arrayWithObjects:
                               [UIImage imageNamed:@"01 Home"],
                               [UIImage imageNamed:@"01 Home2"],
                               nil];
        img.image = [UIImage imageNamed:@"01 Home"];
        
        img.animationRepeatCount = 500;
        img.animationDuration = 0.5;
        [self.view addSubview:img];
        [self.view addSubview:startBtn];
        
        [img startAnimating];
    }];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [NSThread sleepForTimeInterval:0.1];
}

@end
