//
//  AppDelegate.h
//  choice_2
//
//  Created by hgu on 2016. 8. 11..
//  Copyright © 2016년 hgu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

