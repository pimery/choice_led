//
//  OutroViewController.m
//  choice_2
//
//  Created by hgu on 2016. 8. 22..
//  Copyright © 2016년 hgu. All rights reserved.
//

#import "OutroViewController.h"
#import "FirstViewController.h"

@interface OutroViewController ()

@end

@implementation OutroViewController
@synthesize endBtn, imgView;

- (void)viewDidLoad {
    [super viewDidLoad];
    endBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    imgView.image = [UIImage imageNamed:@"07 byebye"];
    
    self.host = [[NSUserDefaults standardUserDefaults] stringForKey:@"USER_HOST"];
    
    [self initNetworkCommunication];
    _connectInProgress = YES;
    [self performSelectorInBackground:@selector(waitForConnection:) withObject:nil];
}

-(void) viewDidAppear:(BOOL)animated {
    
    [NSThread sleepForTimeInterval:2];
    
    while(!_connected) {}
}

- (IBAction)btnClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void) waitForConnection:(id) sender {
    @autoreleasepool {
        _connected = false;
        while ([_outputStream streamStatus] != NSStreamStatusOpen && _connectInProgress) {
        }
        if (_connectInProgress) {
            NSLog(@"Connected to %@:%@", self.host, PORT);
            _connected = true;
            NSData *data = [[NSData alloc] initWithData:[@"G" dataUsingEncoding:NSASCIIStringEncoding]];
            [_outputStream write:[data bytes] maxLength:[data length]];
        } else {
            NSLog(@"Not connected");
            [self failed];
        }
    }
}

- (void)initNetworkCommunication {
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)CFBridgingRetain(self.host), [PORT intValue], &readStream, &writeStream);
    _outputStream = (NSOutputStream *)CFBridgingRelease(writeStream);
    [_outputStream setDelegate:self];
    [_outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [_outputStream open];
    NSLog(@"init network with %@:%@", self.host, PORT);
}

- (void)failed {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"서버와 연결할 수 없습니다." message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
    }];
    [alert addAction:okAction];
    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc presentViewController:alert animated:YES completion:nil];
}

@end
