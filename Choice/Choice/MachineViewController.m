//
//  MachineViewController.m
//  choice_2
//
//  Created by hgu on 2016. 8. 22..
//  Copyright © 2016년 hgu. All rights reserved.
//

#import "MachineViewController.h"

@interface MachineViewController ()

@end

@implementation MachineViewController
@synthesize endBtn, imgView;

- (void)viewDidLoad {
     [super viewDidLoad];
    
    NSString *imgName = @"06 machine_type";
    NSString *imgName_pressed;
    imgName = [imgName stringByAppendingString:self.resultAlpha];
    //imgName = [imgName stringByAppendingString:@"A"];
    imgName_pressed = [imgName stringByAppendingString:@"_press"];
    
    endBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;

    // check ; animation thread 
    [UIView animateWithDuration:0.1f animations:^{
        imgView.animationImages = [NSArray arrayWithObjects:
                               [UIImage imageNamed:imgName],
                               [UIImage imageNamed:imgName_pressed],
                               nil];
        imgView.image = [UIImage imageNamed:imgName];
        
        imgView.animationRepeatCount = 10;
        imgView.animationDuration = 0.5;
        [self.view addSubview:imgView];
        [self.view addSubview:endBtn];
        
        [imgView startAnimating];
    }];
}

@end
