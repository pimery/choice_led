//
//  ViewController.m
//  choice_2
//
//  Created by hgu on 2016. 8. 11..
//  Copyright © 2016년 hgu. All rights reserved.
//

#import "ViewController.h"
#import "ResultViewController.h"
#define endOfQst 5

@interface ViewController ()

@end

@implementation ViewController
@synthesize question, list, numCycle, alphCycle, yesBtn, noBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    list = [NSMutableArray arrayWithObjects:@"A", @"B", @"C", @"D", @"E", @"F", nil];
    alphCycle = 0; numCycle = 1;   // 아래쪽 인덱싱때문에 한번 빼줘야함
    
    NSString *imgName = self.EorB;
    imgName = [imgName stringByAppendingString:[NSString stringWithFormat:@"_%@_", [list objectAtIndex:alphCycle]] ];
    imgName = [imgName stringByAppendingString:[NSString stringWithFormat:@"%ld", (long)numCycle]];
    
    //question.image =[UIImage imageNamed:imgName];
    NSString *path = [[NSBundle mainBundle] pathForResource:imgName ofType:@"png"];
    UIImage *image = [[UIImage alloc] initWithContentsOfFile:path];
    question.image = image;
    image = nil;
    
    
    yesBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    noBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:yesBtn];
    [self.view addSubview:noBtn];
    
}

- (IBAction)btnClicked:(id)sender {
    
    if(sender == noBtn) {
        [list removeObjectAtIndex:alphCycle];
        alphCycle--;
    }
    
    // 클릭 후 다음 이미지 출력
    if( (list.count == 1) ) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ResultViewController *view = [storyboard instantiateViewControllerWithIdentifier:@"ResultView"];
        view.resultAlpha = [list objectAtIndex:0];
        
        [NSThread sleepForTimeInterval:0.05];
        [self.navigationController pushViewController:view animated:YES];
    }
    else if( (list.count !=0 )&&( numCycle== endOfQst )&&( (alphCycle+1) == list.count ) ) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ResultViewController *view = [storyboard instantiateViewControllerWithIdentifier:@"ResultView"];
        view.resultAlpha = [list objectAtIndex:0];
        
        [NSThread sleepForTimeInterval:0.05];
        [self.navigationController pushViewController:view animated:YES];
    }
    else {
        if( (alphCycle+1) >= list.count ) {
            numCycle++; alphCycle = 0;      // initializing
        }
        else {
            alphCycle++;
        }

    NSString *imgName = self.EorB;
    imgName= [imgName stringByAppendingString:[NSString stringWithFormat:@"_%@_", [list objectAtIndex:alphCycle]]];
    imgName = [imgName stringByAppendingString:[NSString stringWithFormat:@"%ld", (long)numCycle]];
    
    //question.image =[UIImage imageNamed:imgName];
    NSString *path = [[NSBundle mainBundle] pathForResource:imgName ofType:@"png"];
    UIImage *image = [[UIImage alloc] initWithContentsOfFile:path];
    question.image = image;
    image = nil;

    }
}


@end
