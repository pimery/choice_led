//
//  OutroViewController.h
//  choice_2
//
//  Created by hgu on 2016. 8. 22..
//  Copyright © 2016년 hgu. All rights reserved.
//
#define PORT @"4000"

#import <UIKit/UIKit.h>

@interface OutroViewController : UIViewController <NSStreamDelegate> {
    BOOL _connectInProgress;
    BOOL _connected;
    NSOutputStream *_outputStream;
}
@property NSString *host;
@property (weak, nonatomic) IBOutlet UIButton *endBtn;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
- (IBAction)btnClicked:(id)sender;

@end
