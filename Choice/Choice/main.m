//
//  main.m
//  choice_2
//
//  Created by hgu on 2016. 8. 11..
//  Copyright © 2016년 hgu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
