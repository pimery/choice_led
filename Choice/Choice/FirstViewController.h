//
//  FirstViewController.h
//  choice_2
//
//  Created by hgu on 2016. 8. 14..
//  Copyright © 2016년 hgu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UIButton *startBtn;

//- (IBAction)btnClicked:(id)sender;

@end
