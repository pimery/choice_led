//
//  MachineViewController.h
//  choice_2
//
//  Created by hgu on 2016. 8. 22..
//  Copyright © 2016년 hgu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreBluetooth/CoreBluetooth.h"

@interface MachineViewController : UIViewController

@property NSString *resultAlpha;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIButton *endBtn;
@end
