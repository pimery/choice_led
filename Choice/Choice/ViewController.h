//
//  ViewController.h
//  choice_2
//
//  Created by hgu on 2016. 8. 11..
//  Copyright © 2016년 hgu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
//@property (weak, nonatomic) IBOutlet UILabel *question;

@property (weak, nonatomic) IBOutlet UIImageView *question;
@property (weak, nonatomic) IBOutlet UIButton *yesBtn;
@property (weak, nonatomic) IBOutlet UIButton *noBtn;

- (IBAction)btnClicked:(id)sender;

@property NSMutableArray *list;
@property NSInteger numCycle;
@property NSInteger alphCycle;
@property NSString *EorB;

@end

