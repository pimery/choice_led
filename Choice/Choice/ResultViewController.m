//
//  ResultViewController.m
//  choice_2
//
//  Created by hgu on 2016. 8. 15..
//  Copyright © 2016년 hgu. All rights reserved.
//

#import "ResultViewController.h"
#import "MachineViewController.h"

@interface ResultViewController ()

@end

@implementation ResultViewController


- (void)viewDidLoad {
    [super viewDidLoad];

    NSString *imgName = [@"05 _type " stringByAppendingString:self.resultAlpha];
    self.imgView.image = [UIImage imageNamed:imgName];
    
    self.host = [[NSUserDefaults standardUserDefaults] stringForKey:@"USER_HOST"];
    
    [self initNetworkCommunication];
    _connectInProgress = YES;
    [self performSelectorInBackground:@selector(waitForConnection:) withObject:nil];

//      ui스레드는 메인 스레드라서 sleep 걸어두면 안된다
//    UITapGestureRecognizer *singleFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
//    [self.view addGestureRecognizer:singleFingerTap];
    
}

-(void) viewDidAppear:(BOOL)animated {

    [NSThread sleepForTimeInterval:2];
    
    while(!_connected) {}
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MachineViewController *view = [storyboard instantiateViewControllerWithIdentifier:@"MachineView"];
    view.resultAlpha = self.resultAlpha;
    [self send];
    [self.navigationController pushViewController:view animated:YES];
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    if (!_connected) {
        [self failed];
        return;
    }
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MachineViewController *view = [storyboard instantiateViewControllerWithIdentifier:@"MachineView"];
    view.resultAlpha = self.resultAlpha;
    [self send];
    [self.navigationController pushViewController:view animated:YES];
}

- (void) waitForConnection:(id) sender {
    @autoreleasepool {
        _connected = false;
        while ([_outputStream streamStatus] != NSStreamStatusOpen && _connectInProgress) {
        }
        if (_connectInProgress) {
            NSLog(@"Connected to %@:%@", self.host, PORT);
            _connected = true;
        } else {
            NSLog(@"Not connected");
            [self failed];
        }
    }
}


- (void)initNetworkCommunication {
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)CFBridgingRetain(self.host), [PORT intValue], &readStream, &writeStream);
    _outputStream = (NSOutputStream *)CFBridgingRelease(writeStream);
    [_outputStream setDelegate:self];
    [_outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [_outputStream open];
    NSLog(@"init network with %@:%@", self.host, PORT);
}

- (void)failed {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"서버와 연결할 수 없습니다." message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"확인" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
    }];
    [alert addAction:okAction];
    UIViewController *vc = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [vc presentViewController:alert animated:YES completion:nil];
}

-(void) send {
    NSData *data = [[NSData alloc] initWithData:[self.resultAlpha dataUsingEncoding:NSASCIIStringEncoding]];
    [_outputStream write:[data bytes] maxLength:[data length]];
}

@end
