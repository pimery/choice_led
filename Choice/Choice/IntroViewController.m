//
//  IntroViewController.m
//  choice_2
//
//  Created by hgu on 2016. 8. 22..
//  Copyright © 2016년 hgu. All rights reserved.
//

#import "IntroViewController.h"


@interface IntroViewController ()

@end

@implementation IntroViewController
@synthesize startBtn, img;

- (void)viewDidLoad {
    [super viewDidLoad];
    startBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [NSThread sleepForTimeInterval:0.1];
}

@end
