//
//  IntroViewController.h
//  choice_2
//
//  Created by hgu on 2016. 8. 22..
//  Copyright © 2016년 hgu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UIButton *startBtn;

@end
