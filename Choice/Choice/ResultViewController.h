//
//  ResultViewController.h
//  choice_2
//
//  Created by hgu on 2016. 8. 15..
//  Copyright © 2016년 hgu. All rights reserved.
//

#define PORT @"4000"

#import <UIKit/UIKit.h>

@interface ResultViewController : UIViewController <NSStreamDelegate> {
    BOOL _connectInProgress;
    BOOL _connected;
    NSOutputStream *_outputStream;
}

@property NSString *resultAlpha;
@property NSString *host;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@end
