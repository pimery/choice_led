#include <SoftwareSerial.h>
#include <Adafruit_NeoPixel.h>

#define START_PIN 8
#define LED_PER 8
#define LED_LINE 2
#define LED_LINE_PER 3
#define LED_TOTAL 6

Adafruit_NeoPixel led_line[LED_LINE];
//SoftwareSerial BTSerial(2, 3);
int current = 0, index = -1;
char val; bool valid = false;

void setup() {
  Serial.begin(9600);
  Serial.println("Hello!");
  //BTSerial.begin(9600);
  for (int i = 0; i < LED_LINE; i++) {
    led_line[i] = Adafruit_NeoPixel(LED_PER * LED_LINE_PER, START_PIN + i, NEO_GRB + NEO_KHZ800);
    led_line[i].begin();
  }
}

void loop() {
  //if (BTSerial.available()) {
    //val = BTSerial.read();
  if (Serial.available()) {
    val = Serial.read();
    current = atoi(&val);
    Serial.println(current);

    if (current < 0 || current >= LED_TOTAL) {
      Serial.println("wrong num");
      valid = false;
    } else {
      valid = true;
    }

    for (int i = 0; i < LED_LINE; i++) {
      for (int j = 0; j < LED_PER * LED_LINE_PER; j++) {
        index = current % LED_LINE_PER;
        if (valid && ((current / LED_LINE_PER) == i) && (j >= index * LED_PER) && (j < (index + 1) * LED_PER)) {
          led_line[i].setPixelColor(j, 0xFFFFFF);
          Serial.print(1);
        } else {
          led_line[i].setPixelColor(j, 0);
          Serial.print(0);
        }
        if ((j+1) % LED_PER == 0) Serial.print(" ");
      }
      led_line[i].show();
      Serial.println();
    }
  }
}

